#!/bin/bash
set -e

export TURTLEBOT3_MODEL=waffle
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/opt/ros/rolling/share/turtlebot3_gazebo/models

# Source the ros2 environment setup
source /opt/ros2_ws/install/setup.bash

# Source the Gazebo environment setup to avoid assertion error in gz-client
source /usr/share/gazebo/setup.bash

# activate the colcon autocomplete
source /usr/share/colcon_argcomplete/hook/colcon-argcomplete.bash

# It's worth noting that exec "$@" is often used in combination with the -e option,
# which sets the exit code of the script to the exit code of the last command executed.
# This allows you to propagate errors from the command line to the script and handle them appropriately.
exec "$@"
