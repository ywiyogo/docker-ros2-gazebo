# Docker Container for ROS2 Rolling with Gazebo, SLAM Toolbox and Navigation2

This repository provides a Docker container for starting the ROS2 Rolling with the latest Gazebo using a Docker container on Ubuntu 22.04. Currently, it installs the Gazebo 11. The container includes the slam_toolbox and the navigation2 packages to allow developers directly test the SLAM and the navigation features.

<div align="center">
<img src="./imgs/docker_ros2.gif" alt="Docker ROS2 Gazebo preview" width="60%"/>
</div>

## Build the docker container

Build from the .dockerfile

```
./build.sh
```

## Run the docker container

Run the docker container with

```
./run.sh
```

Then source the ROS2 SLAM setup

```
source /opt/ros2_ws/install/setup.bash
```

Start gazebo without ros2

```
gazebo /usr/share/gazebo-11/worlds/pioneer2dx.world
```

If you want to start Gazebo with ROS2 interaction. We need to start ros2 with its gazebo plugins. There are two steps:

1. Start ROS2 with launching Gazebo:

   ```
   ros2 launch gazebo_ros gazebo.launch.py world:=/usr/share/gazebo-11/worlds/pioneer2dx.world
   ```

2. Spawning a robot

   ```
   ros2 run gazebo_ros spawn_entity.py -topic <topic_name> -entity <robot_name>
   ```

Starting the SLAM and Navigation2:

```
ros2 launch nav2_bringup tb3_simulation_launch.py slam:=True
```
