#!/bin/bash
# Author: Yongkie Wiyogo
# Description: Docker builder for Gazebo simulator

container_name="wiyogo/ros2_gazebo_slam"
docker_file="ros2_gazebo.dockerfile"

# Add $1 to add any additional argument such as --no-cache
docker build \
  -t $container_name . \
  -f $docker_file $1
