# Author: Yongkie Wiyogo
# Description: Dockerfile for creating a container with ROS2 Rolling and the latest Gazebo

FROM nvidia/cuda:12.2.0-base-ubuntu22.04

ENV TZ=Europe/Berlin DEBIAN_FRONTEND=noninteractive

# Set X11 display
ENV DISPLAY x11_display

# Nvidia variables
ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

# Install basic utilities
RUN apt update -y --fix-missing && apt install -y apt-utils \
    locales \
    git \
    pip \
    curl \
    gnupg gnupg2 \
    lsb-release \
    software-properties-common

# Installation based from ROS2 rolling documentation https://docs.ros.org/en/rolling/Installation/Ubuntu-Install-Debians.html
# Set the locale to include UTF-8. UTF-8 compatible locales are needed for install
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# install ROS2:rolling dependencies
RUN apt install -y  && apt-add-repository universe
RUN curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null

ENV ROS_DISTRO rolling

# install ROS development tools, packages for the tutorial, and the gazebo plugin for ros2
RUN apt update && apt upgrade -y \
    && apt install -y ros-$ROS_DISTRO-desktop \
    ros-dev-tools \
    ros-$ROS_DISTRO-turtlesim \
    ros-$ROS_DISTRO-rqt* \
    ros-$ROS_DISTRO-rviz2 \
    ros-$ROS_DISTRO-gazebo-ros-pkgs

# Install Gazebo for the suitable ROS version
RUN curl -sSL http://get.gazebosim.org | sh

# install Linux tools and extensions
RUN apt install -y nano \
    wget tree \
    python3-pip python3-colcon-common-extensions python3-vcstool python3-rosdep \
    clang-format \
    ccache lcov lld \
    bash-completion \
    gdb valgrind kcachegrind \
    heaptrack hotspot

# install python packages
RUN pip3 install -U \
    argcomplete \
    rosbags

# Clone source of navigation2, slam_toolbox and turtlebot3
RUN mkdir -p /opt/ros2_ws/src && cd /opt/ros2_ws/src \
    && git clone https://github.com/ros-planning/navigation2.git \
    && git clone https://github.com/SteveMacenski/slam_toolbox.git \
    && git clone https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git -b ros2

# make bash default to run the setup.bash
SHELL ["/bin/bash", "-c"]

# bootstrap rosdep
RUN . /opt/ros/rolling/setup.bash && cd /opt/ros2_ws \
    && rosdep init && rosdep update && rosdep install -y -r --from-paths src --ignore-src

RUN . /opt/ros/rolling/setup.bash && cd /opt/ros2_ws \
    && colcon build --symlink-install

# install nav2 demo dependencies
RUN apt-get update && apt-get install -y \
    ros-$ROS_DISTRO-aws-robomaker-small-warehouse-world


# source overlay from entrypoint
COPY ./ros2_entrypoint.sh /
RUN chmod +x /ros2_entrypoint.sh
ENTRYPOINT ["/ros2_entrypoint.sh"]