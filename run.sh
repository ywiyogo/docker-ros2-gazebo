#!/bin/bash
# Author: Yongkie Wiyogo
# Description: Docker runner for ROS2 Rolling and Gazebo simulator

workspace_folder=""
if [ -z "$1" ]; then
  container_name="wiyogo/ros2_gazebo_slam"
else
  container_name=$1
fi

xauth=/tmp/docker.xauth
if [ ! -f "$xauth" ]; then
  touch $xauth
  echo "$xauth is created"
fi

# To work with the GUI we need to set DISPLAY, QT_X11_NO_MITSHM, and mount /tmp/.X11-unix
# Mount the home user and the passwd to allow working as the same user
# Mount the share memory /dev/shm to allow the ROS2 communication between the container.
docker run \
  --rm \
  --net=host \
  --gpus all \
  --user $(id -u):$(id -g) \
  --tmpfs /tmp \
  -e USER=$USER \
  -e HOME=$HOME \
  -e ROS_DISTRO=rolling -e DISPLAY=$DISPLAY \
  -e QT_X11_NO_MITSHM=1 \
  -v /tmp/.X11-unix/:/tmp/.X11-unix:rw \
  -e XAUTHORITY=$xauth \
  -e "TERM=xterm-256color" \
  -h $HOSTNAME \
  -v /home/$USER:/home/$USER \
  -v /etc/passwd:/etc/passwd:ro \
  -v /etc/group:/etc/group:ro \
  -v /dev/shm:/dev/shm \
  -w /home/$USER \
  -it $container_name /bin/bash
